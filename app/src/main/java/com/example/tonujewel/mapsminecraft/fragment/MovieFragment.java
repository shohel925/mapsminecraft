package com.example.tonujewel.mapsminecraft.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tonujewel.mapsminecraft.R;

import java.util.ArrayList;
import java.util.List;

public class MovieFragment extends BaseFragment {

	private Context context;

	private View view;
	List<String >temp=new ArrayList<>();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.activity_main, container, false);
		context=getActivity();
		initUI();
		return view;
	}

	private void initUI() {
		temp.add("ADVENTURE");
		temp.add("ADVENTURE2");
		temp.add("ADVENTURE3");
		temp.add("ADVENTURE4");
		temp.add("ADVENTURE5");
		TabsPagerAdapter adapter = new TabsPagerAdapter(getChildFragmentManager());
		ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
		pager.setAdapter(adapter);

	}

	public class TabsPagerAdapter extends FragmentPagerAdapter {

		public TabsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return new MovieListFragment();
		}

		@Override
		public int getCount() {
			return temp.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return temp.get(position);
		}
	}

}
