package com.example.tonujewel.mapsminecraft.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tonujewel.mapsminecraft.R;
import com.example.tonujewel.mapsminecraft.adapter.MovieAdapter;
import com.example.tonujewel.mapsminecraft.model.MovieDM;

import java.util.ArrayList;
import java.util.List;

public class MovieListFragment extends BaseFragment {

	private Context context;

	private View view;
	private RecyclerView movieList;
	private MovieAdapter movieAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.movie_list, container, false);
		context=getActivity();
		initUI();
		return view;
	}

	private void initUI() {
		 List<MovieDM>tempMovie=new ArrayList<>();
		tempMovie.clear();
		for (int i=0; i<5; i++){
			MovieDM movieDM=new MovieDM();
			movieDM.setTitle("Movie"+i);
			tempMovie.add(movieDM);

		}
		movieList=(RecyclerView)view.findViewById(R.id.movieList);
		movieAdapter = new MovieAdapter(getActivity(),tempMovie,null);
		LinearLayoutManager llm = new LinearLayoutManager(context);
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		movieList.setLayoutManager(llm);
		movieList.setItemAnimator(new DefaultItemAnimator());
		movieList.setAdapter(movieAdapter);
		
	}

}
