package com.example.tonujewel.mapsminecraft.adapter;

import android.app.Activity;
import android.app.Notification;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tonujewel.mapsminecraft.R;
import com.example.tonujewel.mapsminecraft.model.MovieDM;
import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {
    Activity context;
    List<MovieDM> notificationList= new ArrayList<>();
    private final AdapterView.OnItemClickListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {

           private TextView title,premiumTv;


        public MyViewHolder(View view) {
            super(view);
            premiumTv=(TextView)view.findViewById(R.id.premiumTv);
            title = (TextView) view.findViewById(R.id.title);
        }

        public void bind(final MovieDM item, final AdapterView.OnItemClickListener listener) {



            if (!TextUtils.isEmpty(item.getTitle())){
                title.setText(item.getTitle());
            }else{
                title.setText("");
            }
            premiumTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context,""+item.getTitle(),Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    public MovieAdapter(Activity context, List<MovieDM> notificationList, AdapterView.OnItemClickListener listener) {
        this.context = context;
        this.notificationList = notificationList;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_rows, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(notificationList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return notificationList == null ? 0 : notificationList.size();
    }

}
