package com.example.tonujewel.mapsminecraft.model;

/**
 * Created by Tonujewel on 10/18/2016.
 */

public class MovieDM {
    private String title="";
    private String imageUrl="";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
