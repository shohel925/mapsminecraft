package com.example.tonujewel.mapsminecraft.fragment;


import android.support.v4.app.Fragment;

public interface OnFragmentInteractionListener {

	public void setContentFragment(Fragment fragment, boolean addToBackStack);
}
